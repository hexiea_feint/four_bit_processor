# FourBitProcessor

*This will be a simple elixir emulation of a 4 bit processor with two 4 bit registers.*

## Design

* **Data Registers** are 4 bits and have these control signals:
  * clock  
  * load (when high, loads value from bus on the rising edge of the next clock pulse)
  * clear (set value to 0)
  * output enable (puts its value onto the data bus)

* **Data Bus**
  * This is just 4 bits; anybody connected to this bus can read the value on the bus, or
  * place a value on the bus

* **Processor**
  * A driver that creates registers A and B and a data bus, and then signals registers to place values onto the bus and read values off of the bus.

### Data Register *A*, *B*
* **Agent**
  * `clock(:high|:low) -> :ok`
  * `clock() -> :high | :low`
  * `load(:high|:low) -> :ok`
  * `load() -> :high | :low`
  * `clear(:high|:low) -> :ok`
  * `clear() -> :high | :low`
  * `output_enable(:high|:low) -> :ok`
  * `output_enable() :high | :low`
  * `pin_state() -> <<state::size(4)>>`
  * `pin_state(<<state::size(4)>>) -> :ok`
  * `data() -> <<data::size(4)>>`
  * `data(<<data::size(4)>>) = :ok`

### Data Bus
* **Agent**
  * `state() -> <<state::size(4)>>`
  * `state(<<state::size(4)>>) -> :ok`
  * `clear()`

### Clock
* **Agent**
  * `start_link(pulse_time, type) #creates the process`
    * `type: [:high, :low, :both]`
  * `start() #actually starts the clock`
    * `cycle(state)`
  * `cycle(state)`
    * `sleep(pulse_time)`
    * `send type`
    * `cycle(state)`
  * `stop() -> :ok`

### Processor
* **Agent**
  * `start()`
  * `cycle(state = {%Register{}, %Register{}, %Bus{})`
    * `receive tick`
      * `{:ok, edge}`
        * `next_state = resolve(edge, state)`
    * `cycle(next_state)`
  * `resolve(:high, {register_a, register_b, bus})`
  * `resolve(:low, {register_a, register_b, bus})`
  * `load(register|bus, data)`
  * `read(register|bus)`

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `four_bit_processor` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:four_bit_processor, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/four_bit_processor](https://hexdocs.pm/four_bit_processor).
