defmodule FourBitProcessor.Register do
  use Agent
  use Bitwise, only_operators: true
  @init <<0::4>>

  def start_link(_opts \\ []) do
    Agent.start_link(fn -> %{control: @init, data: @init} end)
  end

  def clock(register, state) do
    update_pin(register, :clock, state)
    :ok
  end

  def clock(register) do
    <<current::1, _::1, _::1, _::1>> = pin_state(register)
    current
  end

  def load(register, state) do
    update_pin(register, :load, state)
    :ok
  end

  def load(register) do
    <<_::1, current::1, _::1, _::1>> = pin_state(register)
    current
  end

  def clear(register, state) do
    update_pin(register, :clear, state)
    :ok
  end

  def clear(register) do
    <<_::1, _::1, current::1, _::1>> = pin_state(register)
    current
  end

  def output_enable(register, state) do
    update_pin(register, :output, state)
    :ok
  end

  def output_enable(register) do
    <<_::1, _::1, _::1, current::1>> = pin_state(register)
    current
  end

  def pin_state(register, state = <<_::4>>) do
    Agent.update(register, fn current -> %{current | control: state} end)
  end

  def pin_state(_, _), do: raise ArgumentError, "overflow"

  def pin_state(register) do
    Agent.get(register, fn %{control: control} -> control end)
  end

  defp update_pin(register, pin, bit) do
    <<clk::1, ld::1, clr::1, out::1>> = pin_state(register)
    case pin do
      :clock -> pin_state(register, <<bit::1, ld::1, clr::1, out::1>>)
      :load -> pin_state(register, <<clk::1, bit::1, clr::1, out::1>>)
      :clear -> pin_state(register, <<clk::1, ld::1, bit::1, out::1>>)
      :output -> pin_state(register, <<clk::1, ld::1, clr::1, bit::1>>)
    end
  end

  def data(register, data = <<_::4>>) do
    Agent.update(register, fn current -> %{current | data: data} end)
  end

  def data(_, _), do: raise ArgumentError, "overflow"

  def data(register) do
    Agent.get(register, fn %{data: data} -> data end)
  end

end
