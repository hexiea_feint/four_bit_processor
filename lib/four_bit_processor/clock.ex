defmodule FourBitProcessor.Clock do
  use Task

  def start_link(speed: speed, type: type, target: target)
    when speed > 0 do
    Task.start FourBitProcessor.Clock, :wait_for_start, [%{speed: speed, type: type, target: target, tick: true}]
  end

  def start_link(_), do: raise ArgumentError, "Speed of 0 or less not allowed"

  def start(clock) do
    send clock, :start
    :started
  end

  def stop(clock) do
    Process.exit(clock, :kill)
    :stopped
  end

  def cycle(state = %{speed: speed, type: type, target: target, tick: tick}) do
    Process.sleep(speed)
    case {type, tick} do
      {:low, false} -> send target, :low
      {:high, true} -> send target, :high
      {:both, _} -> send target, tick_to_atom(tick)
      {_, _} -> :ok
    end
    cycle(%{state | tick: !tick})
  end

  def wait_for_start(args) do
    receive do
      :start ->
        cycle(args)
    end
  end

  defp tick_to_atom(false), do: :low
  defp tick_to_atom(true), do: :high
end
