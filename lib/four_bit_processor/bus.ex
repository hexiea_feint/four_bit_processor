defmodule FourBitProcessor.Bus do
  use Agent
  @init <<0::4>>

  def start_link(_opts \\ []) do
    Agent.start_link(fn -> @init end)
  end

  def state(agent) do
    Agent.get(agent, &(&1))
  end

  def state(agent, state_to_set = <<_::4>>) do
    Agent.update(agent, fn _state -> state_to_set end)
  end

  def state(_, _), do: raise ArgumentError, "overflow"

  def clear(agent) do
    Agent.update(agent, fn _state -> @init end)
  end
end
