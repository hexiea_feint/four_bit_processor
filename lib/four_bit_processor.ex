defmodule FourBitProcessor do
  @moduledoc """
  Documentation for `FourBitProcessor`.
  """
  use Agent
  
  @typedoc """
  Processor Specification
  """
  @type proc_spec :: %{
    register_a: pid,
    register_b: pid,
    bus: pid,
    clock: pid
  }

  def start_link(_opts) do
  end

  def cycle(state) do
  end

  defp resolve(:high, state = {register, register, bus}) do
  end

  defp resolve(:low, state) do
  end

  def load(register, data) do
  end

  def load(bus, data) do
  end

  def read(register) do
  end

  def read(bus) do
  end
end
