defmodule FourBitProcessor.RegisterTest do
  use ExUnit.Case
  alias FourBitProcessor.Register

  # doctest Register

  describe "After a register is initialized" do
    setup [:initialize_register]

    test "it should spawn a new process", %{register: register} do
      assert Process.info(register) != nil
    end

    test "its clock pin can be set", %{register: register} do
      assert Register.clock(register, 1) == :ok
      assert Register.clock(register) == 1
      assert Register.clock(register, 0) == :ok
      assert Register.clock(register) == 0
    end

    test "its load pin can be set", %{register: register} do
      assert Register.load(register, 1) == :ok
      assert Register.load(register) == 1
      assert Register.load(register, 0) == :ok
      assert Register.load(register) == 0
    end

    test "its clear pin can be set", %{register: register} do
      assert Register.clear(register, 1) == :ok
      assert Register.clear(register) == 1
      assert Register.clear(register, 0) == :ok
      assert Register.clear(register) == 0
    end

    test "its output enable pin can be set", %{register: register} do
      assert Register.output_enable(register, 1) == :ok
      assert Register.output_enable(register) == 1
      assert Register.output_enable(register, 0) == :ok
      assert Register.output_enable(register) == 0
    end

    test "its pin state can be retreived", %{register: register} do
      Register.output_enable(register, 1)
      Register.clock(register, 1)
      assert Register.pin_state(register) == <<1::1, 0::1, 0::1, 1::1>>
      Register.clock(register, 0)
      assert Register.pin_state(register) == <<0::1, 0::1, 0::1, 1::1>>
    end

    test "its data can be stored", %{register: register} do
      assert Register.data(register) == <<0::4>>
      assert Register.data(register, <<7::4>>) == :ok
      assert Register.data(register) == <<7::4>>
    end
  end

  defp initialize_register(_context) do
    {:ok, register} = Register.start_link()
    %{register: register}
  end
end
