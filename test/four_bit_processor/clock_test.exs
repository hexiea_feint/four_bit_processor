defmodule FourBitProcessor.ClockTest do
  use ExUnit.Case
  alias FourBitProcessor.Clock

  # in milliseconds
  @time_max_error 20
  # in milliseconds
  @default_clock_speed 100
  @default_tick :both

  # doctest Clock

  describe "when the clock is initialized" do
    test "a pulse time of 0 or less shouldn't be allowed" do
      assert_raise(ArgumentError, fn ->
        Clock.start_link(speed: 0, type: @default_tick, target: self())
      end)

      assert_raise(ArgumentError, fn ->
        Clock.start_link(speed: -500, type: @default_tick, target: self())
      end)
    end

    test "a new process should be created" do
      {:ok, clock} =
        Clock.start_link(speed: @default_clock_speed, type: @default_tick, target: self())

      assert Process.info(clock) != nil
    end
  end

  describe "when the clock is started" do
    test "with a tick type of :high, a :high message is received" do
      %{clock: clock} = initialize_clock(type: :high)
      assert Clock.start(clock) == :started

      assert_receive :high, @default_clock_speed + @time_max_error
    end

    test "with a tick type of :low, a :low message is received" do
      %{clock: clock} = initialize_clock(type: :low)
      assert Clock.start(clock) == :started

      assert_receive :low, @default_clock_speed * 2 + @time_max_error
    end

    test "with a tick type of :both, both :low and :high messages are received" do
      %{clock: clock} = initialize_clock(type: :both)
      assert Clock.start(clock) == :started

      assert_receive :high, @default_clock_speed + @time_max_error
      assert_receive :low, @default_clock_speed + @time_max_error
    end

    test "a tick message is returned after the pulse time" do
      %{clock: clock} = initialize_clock(type: @default_tick)
      assert Clock.start(clock) == :started
      start_time = Time.utc_now()

      receive do
        tick -> tick
      end

      end_time = Time.utc_now()

      diff =
        start_time
        |> Time.add(@default_clock_speed, :millisecond)
        |> Time.diff(end_time, :millisecond)
        |> abs

      assert diff < @time_max_error
    end
  end

  describe "when the clock stops" do
    test "it is stopped successfully" do
      %{clock: clock} = initialize_clock(type: @default_tick)

      assert Clock.stop(clock) == :stopped
      assert Process.info(clock) == nil
    end
  end

  defp initialize_clock(type: tick_type) do
    {:ok, clock} = Clock.start_link(speed: @default_clock_speed, type: tick_type, target: self())
    %{clock: clock}
  end
end
