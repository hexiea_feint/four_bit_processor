defmodule FourBitProcessor.BusTest do
  use ExUnit.Case
  alias FourBitProcessor.Bus

  # doctest Bus

  describe "after the bus initializes" do
    setup [:initialize_bus]

    test "it should spawn a new process", %{bus: bus} do
      assert Process.info(bus) != nil
    end

    test "its state should start with 0x0", %{bus: bus} do
      assert Bus.state(bus) == <<0::4>>
    end

    test "its state can be set", %{bus: bus} do
      Bus.state(bus, <<6::4>>)
      assert Bus.state(bus) == <<6::4>>
      Bus.state(bus, <<3::4>>)
      assert Bus.state(bus) == <<3::4>>
    end
  end

  describe "when the bus's state is set" do
    setup [:initialize_bus]

    test "it returns an error when it is out of bounds", %{bus: bus} do
      assert_raise(ArgumentError, fn ->
        Bus.state(bus, <<35::9>>)
      end)

      assert_raise(ArgumentError, fn ->
        Bus.state(bus, <<-45::3>>)
      end)
    end
  end

  describe "when the bus is cleared" do
    setup [:initialize_bus]

    test "its state should be 0x0", %{bus: bus} do
      Bus.state(bus, <<6::4>>)
      Bus.clear(bus)
      assert Bus.state(bus) == <<0::4>>
    end
  end

  defp initialize_bus(_context) do
    {:ok, bus} = Bus.start_link([])
    %{bus: bus}
  end
end
